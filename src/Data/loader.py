import numpy as np
import logging


def collect_data(data_path: str, logger: logging.Logger) -> np.ndarray:
    """
    simply extract data and convert from data_path.
    the output of this function is a tuple of two lists.
    The first list contains the data (X) which are numpy
    array. The second list contains the labels (Y) which
    is a simple digit corresponding to the class.

    In order to have a code that can be used with future
    data extractions, we can assume that the folder contains .txt files
    and one json file containing a dict which maps file names
    to their corresponding class (see readme.md for more details).

    Args:
        data_path: path to the raw data
        logger: logger for verbose recording
    """
    raise NotImplementedError("function to implement")
