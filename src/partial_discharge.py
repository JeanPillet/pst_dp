"""
This project studies partial discharges in the context of a PST
at Centrale-Supelec.
"""

if __name__ == "__main__":
    import os

    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
    import argparse

    parser = argparse.ArgumentParser(
        usage=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument(
        "--logger",
        nargs="?",
        type=str,
        default="logs.log",
        help="path to save the logs (if you use the default value the file will be overwritten)",
    )
    parser.add_argument(
        "--path",
        nargs="?",
        type=str,
        default="raw_data",
        help="path to the raw data",
    )
    parser.add_argument(
        "--create_json",
        action="store_true",
        default=False,
        help="run the unit test",
    )
    args = parser.parse_args()
    from .utils.GetLogger import get_logger
    from .utils.Clear import remove_chache_folders
    from .utils.Packages import check_packages

    logger = get_logger(log_file=args.logger)
    check_packages(logger=logger)
    if args.create_json:
        from .utils.JsonCreator import create_json_for_dataset

        create_json_for_dataset(data_path=args.path, logger=logger)
        remove_chache_folders()
        quit()
    import logging
    from .Data.loader import collect_data


def train_a_neural_netwrok(data_path: str, logger: logging.Logger) -> None:
    """
    This function wille be updated through time.
    We need a path to the raw data fodler from which we will 1) extract
    the data, 2) pre-process it 3) create a train set and test set.
    Then, we define the network to train and its task.
    Finally we implement the training loop before saving the final product.

    Args:
        data_path: path to the raw data
        logger: logger for verbose recording
    """
    collect_data(data_path=data_path, logger=logger)


if __name__ == "__main__":
    train_a_neural_netwrok(data_path=args.path, logger=logger)
    remove_chache_folders()
